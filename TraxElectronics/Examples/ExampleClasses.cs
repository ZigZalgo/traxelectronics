﻿/*
 * Copyright 2019 VizworX Inc.
 * All rights reserved
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace TraxElectronics
{
    public class Examples
    {
        /// <summary>
        /// A static rng field makes the random decision node reproduceable
        /// </summary>
        public static Random rng = new Random(1337);

        /// <summary>
        /// Sample usage for training a decision tree using 10-fold cross validation
        /// </summary>
        public static void ExampleTraining(string FileDirectory, string TreeDirectory)
        {

            //Here we use the log to indicate we are starting
            Log.Message("Beginning Training...");

            //Tell the parser which directory to read the inputs from
            Parser p = new Parser(FileDirectory);

            //Tell the parser how many files in the directory do you want to sample (leave empty to parse all)
            List<FloatExample> inputs = p.ParseAllList();

            //Encode using 1000 buckets
            Dictionary<float, ushort>[] codeBook = inputs.Encode(1000);

            //Code the inputs using the codebook
            ShortExample[] discrete = inputs.Discretize(codeBook);

            //This step simply clears memory because we don't need to float values anymore
            inputs = null;
            GC.Collect();

            //Shuffle using the static random variable for reproducability
            discrete.Shuffle(rng);

            //crossfold validating (1 test, 9 train sets)
            var splits = discrete.Split(discrete.Count() / 9).ToArray();

            //Temporary best tree as we loop over each fold
            DecisionTree best = null;
            float bestacc = 0;

            //For each fold we need to train/validate
            for (int i = 0; i < splits.Count(); i++)
            {
                Log.Message("Fold: " + i);
                List<ShortExample> training = new List<ShortExample>();
                List<ShortExample> testing = new List<ShortExample>();

                //Split the discretized inputs into the training and testing sets
                for (int j = 0; j < splits.Count(); j++)
                {
                    if (j == i)
                        testing.AddRange(splits[j]);
                    else
                        training.AddRange(splits[j]);
                }

                //learn a decision tree of type Best node, with a max depth, and min bucket size
                DecisionTree tree = new DecisionTree(DecisionTreeType.BestNode, discrete[0].FeatureValues.Length, 1000);

                //Train the program on the folded inputs
                tree.Train(training);

                //Now check how accurate it was
                float count = 0;
                for (int example = 0; example < testing.Count; example++)
                {
                    //Try to predict the curretn example
                    DrillState predicted = tree.Predict(testing[example].FeatureValues, out float prob);
                    //Count how many we got right
                    if (testing[example].Predictor == predicted)
                        count++;
                }
                //Return the total accuracy for this fold
                float acc = count / testing.Count;
                Log.Message("Fold: " + i + " Accuracy: " + acc);

                //If this fold did better, we overwrite our current best
                if (acc > bestacc)
                {
                    best = tree;
                    bestacc = acc;
                    Log.Message("Current best: " + bestacc);
                }
            }
            //Save the best tree
            DecisionTree.Save(best, codeBook, TreeDirectory);

        }

        /// <summary>
        /// Sample usage for validating a previously saved decision tree
        /// </summary>
        public static void ExampleValidation(string FileDirectory, string TreeDirectory)
        {
            //Here we use the log to indicate we are starting
            Log.Message("Beginning Validation...");

            //Tell the parser which directory to read the inputs from
            Parser p = new Parser(FileDirectory);

            //Tell the parser how many files in the directory do you want to sample (leave empty to parse all)
            List<FloatExample> inputs = p.ParseAllList();

            //Load the previously trained decision tree and codebook
            DecisionTree tree = DecisionTree.Load(TreeDirectory, out Dictionary<float, ushort>[] codeBook);

            //Code the inputs using the loaded codebook
            ShortExample[] discrete = inputs.Discretize(codeBook);

            //This step simply clears memory because we don't need to float values anymore
            inputs = null;
            GC.Collect();

            //Here we loop over every example
            int count = 0;
            for (int i = 0; i < discrete.Length; i++)
            {
                //Try to get the tree to predict the drill state
                DrillState state = tree.Predict(discrete[i].FeatureValues, out float prob);

                //If it's right, we increment the count
                if (state == discrete[i].Predictor)
                    count++;
            }

            //Calculate ratio of correct to incorrect guesses.
            float accuracy = count / (float)discrete.Length;

            //Show me the money
            Log.Message("Accuracy: " + accuracy);
            return;
        }

        /// <summary>
        /// Sample usage for predicting a single input example
        /// </summary>
        public static DrillState SamplePrediction(FloatExample inputs, DecisionTree tree, Dictionary<float, ushort>[] codeBook)
        {

            //Predict the coded input (Prob here is how confident the algorithm is)
            return tree.Predict(inputs.Discretize(codeBook).FeatureValues, out float prob);

        }

    }
}

