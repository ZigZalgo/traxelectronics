﻿/*
 * Copyright 2019 VizworX Inc.
 * All rights reserved
 */

using System;
using System.IO;

namespace TraxElectronics
{

    /// <summary>
    /// The different types of logs used for logging
    /// </summary>
    public enum LogTypes
    {
        None = 0,
        Warning = 1,
        Error = 2,
        Exception = 4,
        Message = 8
    }

    /// <summary>
    /// Class for logging all events that occur in eutocius
    /// </summary>
    public static class Log
    {

        #region Fields

        /// <summary>
        /// The format for how to print out the log date times
        /// </summary>
        private const string DATE_FORMAT = "MM/dd/yy|H:mm:ss.ffff|zzz";

        /// <summary>
        /// The stream writer which writes to the log
        /// </summary>
        private static StreamWriter writer;

        /// <summary>
        /// The allowed types of logging
        /// </summary>
        private static LogTypes allowed;

        /// <summary>
        /// Used to check if the stream has been set
        /// </summary>
        public static bool Streaming { get { return writer != null; } }

        /// <summary>
        /// Used to indicate if we additionally want to output to console
        /// </summary>
        public static bool ConsoleStream;

        #endregion

        #region Methods

        /// <summary>
        /// Used to set the allowed types of logs to be written to the stream
        /// </summary>
        /// <param name="types">The allowed log types</param>
        public static void SetLogTypes(LogTypes types)
        {
            allowed = types;
        }

        public static StreamWriter GetWriter()
        {
            return writer;
        }

        /// <summary>
        /// Sets the stream so that logging takes place
        /// <para></para>
        /// If a stream is already occuring, that stream gets closed, and replaced.
        /// <para></para>
        /// By default, all log types are enabled
        /// </summary>
        /// <param name="ToSet">The stream you would like the information to be logged to</param>
        /// <param name="AllowedLoggingTypes">The allowed log types</param>
        public static void SetStream(Stream ToSet, bool outputToConsole = false, LogTypes AllowedLoggingTypes = (LogTypes)15)
        {
            EndStream();
            allowed = AllowedLoggingTypes;
            writer = new StreamWriter(ToSet);
            ConsoleStream = outputToConsole;
        }

        /// <summary>
        /// Stops the stream(if one exists)
        /// </summary>
        public static void EndStream()
        {
            if (writer != null)
            {
                writer.Flush();
                writer.Dispose();
            }
        }

        /// <summary>
        /// Writes a warning to the current log stream
        /// </summary>
        /// <param name="message">The message to be used for this warning</param>
        /// <param name="context">The context should you want this ot be passed</param>
        public static void Warning(string message, object context = null)
        {
            if ((allowed & LogTypes.Warning) == LogTypes.None)
                return;
            if (!Streaming)
                return;
            DateTime now = DateTime.Now;
            string line = now.ToString(DATE_FORMAT) + " - {Warning}||";
            line += "\t" + message;
            if (context != null)
                line += "\n{Context}" + context.ToString();
            if (ConsoleStream)
                Console.WriteLine(line);
            writer.WriteLine(line);
            writer.Flush();
        }

        /// <summary>
        /// Writes an Error to the current log stream
        /// </summary>
        /// <param name="message">The message to be used for this Error</param>
        /// <param name="context">The context should you want this ot be passed</param>
        public static void Error(string message, object context = null)
        {
            if ((allowed & LogTypes.Error) == LogTypes.None)
                return;
            if (!Streaming)
                return;
            DateTime now = DateTime.Now;
            string line = now.ToString(DATE_FORMAT) + " - {Error}||";
            line += "\t" + message;
            if (context != null)
                line += "\n{Context}" + context.ToString();
            if (ConsoleStream)
                Console.WriteLine(line);
            writer.WriteLine(line);
            writer.Flush();
        }

        /// <summary>
        /// Writes an Exception to the current log stream
        /// </summary>
        /// <param name="message">The message to be used for this exception</param>
        /// <param name="context">The context should you want this ot be passed</param>
        public static void Exception(Exception e, object context = null)
        {
            if ((allowed & LogTypes.Exception) == LogTypes.None)
                return;
            if (!Streaming)
                return;
            DateTime now = DateTime.Now;
            string line = now.ToString(DATE_FORMAT) + " - {Exception}||";
            line += "\t" + e.ToString();
            if (context != null)
                line += "\n{Context}" + context.ToString();
            if (ConsoleStream)
                Console.WriteLine(line);
            writer.WriteLine(line);
            writer.Flush();
        }

        /// <summary>
        /// Writes a message to the current log stream
        /// </summary>
        /// <param name="message">The message to be used for this warning</param>
        /// <param name="context">The context should you want this ot be passed</param>
        public static void Message(string message, object context = null)
        {
            if ((allowed & LogTypes.Message) == LogTypes.None)
                return;
            if (!Streaming)
                return;
            DateTime now = DateTime.Now;
            string line = now.ToString(DATE_FORMAT) + " - {Message}||";
            line += "\t" + message;
            if (context != null)
                line += "\n{Context}" + context.ToString();
            if (ConsoleStream)
                Console.WriteLine(line);
            writer.WriteLine(line);
            writer.Flush();
        }

        #endregion
    }
}

