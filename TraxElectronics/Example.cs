﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TraxElectronics
{

    /// <summary>
    /// I really wish there was a better way to do this type handling,
    /// but this is the cleanest I can think of
    /// </summary>
    public abstract class AbstractExample
    {
        public DrillState Predictor;
        public int ID;
    }

    public class FloatExample : AbstractExample
    {
        public float[] FeatureValues;

        public FloatExample(int id, float[] featureValues, DrillState predictor)
        {
            FeatureValues = featureValues;
            this.Predictor = predictor;
            this.ID = id;
        }

        internal static void WriteToStream(FloatExample ex, BinaryWriter writer)
        {
            writer.Write(ex.ID);
            writer.Write((byte)ex.Predictor);
            writer.Write(ex.FeatureValues.Length);
            for(int i = 0; i < ex.FeatureValues.Length; i++)
            {
                writer.Write(ex.FeatureValues[i]);
            }
        }

        internal static FloatExample ReadFromStream(BinaryReader reader)
        {
            int id = reader.ReadInt32();
            DrillState state = (DrillState)reader.ReadByte();
            int featCount = reader.ReadInt32();
            float[] featurevals = new float[featCount];
            for (int i = 0; i < featCount; i++)
            {
                featurevals[i] = reader.ReadSingle();
            }
            return new FloatExample(id, featurevals, state);
        }
    }

    public class ByteExample: AbstractExample
    {
        public byte[] FeatureValues;

        public ByteExample(int id, byte[] featureValues, DrillState predictor)
        {
            FeatureValues = featureValues;
            this.Predictor = predictor;
            this.ID = id;
        }
    }

    public class ShortExample : AbstractExample
    {
        public ushort[] FeatureValues;
        public ShortExample(int id, ushort[] featureValues, DrillState predictor)
        {
            FeatureValues = featureValues;
            this.Predictor = predictor;
            this.ID = id;
        }
    }
}
