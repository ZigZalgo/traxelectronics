﻿/*
 * Copyright 2019 VizworX Inc.
 * All rights reserved
 */
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace TraxElectronics
{
    public enum DecisionTreeType
    {
        BestNode,
        WorstNode,
        RandomNode
    }

    public class DecisionTree
    {

        #region Fields

        /// <summary>
        /// The rng variable used if using random decision tree learning
        /// </summary>
        [JsonIgnore]
        System.Random rng = new Random(1337);

        /// <summary>
        /// The minimum number of elements that can be held in a node before it is forced to divide
        /// </summary>
        [JsonIgnore]
        int MaxBucketSize = 1;

        /// <summary>
        /// The type of learning this tree will try to do
        /// </summary>
        [JsonIgnore]
        DecisionTreeType TreeType = DecisionTreeType.BestNode;

        /// <summary>
        /// The maximum depth a tree will divide to until we decide to terminate division
        /// </summary>
        [JsonIgnore]
        int MaxDepth = 0;

        /// <summary>
        /// The root node of this tree
        /// </summary>
        [JsonProperty]
        public DecisionTreeNode Root = new DecisionTreeNode();

        #endregion

        #region Constructors

        /// <summary>
        /// Default constructor for newtonsoft
        /// </summary>
        public DecisionTree() { }

        /// <summary>
        /// Creates a decision tree with specific parameters
        /// </summary>
        /// <param name="treeType">The type of tree this learner is supposed to be</param>
        /// <param name="maxDepth">The maximum depth the tree will attempt to learn</param>
        /// <param name="BucketSize"></param>
        /// <param name="FeatToPredict"></param>
        /// <param name="rand"></param>
        public DecisionTree(DecisionTreeType treeType, int maxDepth, int BucketSize, Random rand = null)
        {
            Root = new DecisionTreeNode();
            TreeType = treeType;
            MaxDepth = maxDepth;
            MaxBucketSize = BucketSize;
            if (rand != null)
                rng = rand;
        }

        #endregion

        #region IO

        /// <summary>
        /// Loads a decision tree from memory and also returns the codebook
        /// </summary>
        public static DecisionTree Load(string dir, out Dictionary<float, ushort>[] codeBook)
        {
            Log.Message("Loading Decision Tree with Codebook");
            string codePath = Path.Combine(dir, "CodeBook.JSON");
            string treePath = Path.Combine(dir, "Tree.JSON");

            codeBook = JsonConvert.DeserializeObject<Dictionary<float, ushort>[]>(File.ReadAllText(codePath));
            return JsonConvert.DeserializeObject<DecisionTree>(File.ReadAllText(treePath));
        }

        /// <summary>
        /// Saves a decision tree along with its codebook to memory
        /// </summary>
        public static void Save(DecisionTree tree, Dictionary<float, ushort>[] codeBook, string dir)
        {
            Log.Message("Saving Decision Tree with Codebook");
            string codePath = Path.Combine(dir, "CodeBook.JSON");
            string treePath = Path.Combine(dir, "Tree.JSON");
            File.WriteAllText(treePath, JsonConvert.SerializeObject(tree));
            File.WriteAllText(codePath, JsonConvert.SerializeObject(codeBook));
        }

        #endregion

        #region Methods

        /// <summary>
        /// Trains the tree on the given inputs
        /// </summary>
        public void Train(List<ShortExample> Inputs)
        {
            Log.Message("Starting Training on Decision Tree.");
            Log.Message("Tree Type : " + TreeType);
            Log.Message("Max Depth: " + MaxDepth);
            Log.Message("Min Bucket Size: " + MaxBucketSize);
            Root.CurrentGroup = Inputs;
            Root.Divide(MaxBucketSize, MaxDepth, TreeType, rng);
            Log.Message("Done Learning");
        }

        /// <summary>
        /// Predicts the drill state of a given example input. returns the probability as well
        /// </summary>
        public DrillState Predict(ushort[] input, out float prob)
        {
            return Root.Predict(input, out prob);
        }

        /// <summary>
        /// Predicts the drill state of a given example input.
        /// </summary>
        public DrillState Predict(ushort[] input)
        {
            return Root.Predict(input, out float x);
        }

        #endregion

    }

    public class DecisionTreeNode
    {

        #region Fields

        /// <summary>
        /// The current bucket of this node
        /// </summary>
        [JsonIgnore]
        public List<ShortExample> CurrentGroup = new List<ShortExample>();

        /// <summary>
        /// The feature this node was divided on
        /// </summary>
        [JsonProperty]
        private int FeatureDividedOn = -1;

        /// <summary>
        /// The feature value of this node
        /// </summary>
        [JsonProperty]
        private ushort FeatureValueOfGroup;

        /// <summary>
        /// The children nodes
        /// </summary>
        [JsonProperty]
        private List<DecisionTreeNode> Children = new List<DecisionTreeNode>();

        /// <summary>
        /// Whether this node is a leaf or not
        /// </summary>
        [JsonProperty]
        private bool leaf = false;

        /// <summary>
        /// The prediction of this node
        /// </summary>
        [JsonProperty]
        private DrillState Prediction;

        /// <summary>
        /// The confidence of the above prediction
        /// </summary>
        [JsonProperty]
        private float Probability;

        /// <summary>
        /// The depth of this node
        /// </summary>
        [JsonIgnore]
        private int CurrentDepth = 0;

        #endregion

        #region Constructors

        /// <summary>
        /// default JSON constructor
        /// </summary>
        public DecisionTreeNode() { }

        #endregion

        #region Methods

        /// <summary>
        /// Given a discretized input value, recursively predict what drillstate the tree has learned
        /// </summary>
        public DrillState Predict(ushort[] toPredict, out float probability)
        {
            if (leaf == true)
            {
                probability = Probability;
                //If we have a group, the feature value that is most probable is returned
                return Prediction;
            }

            //Otherwise, we find which child to try to predict from
            foreach (DecisionTreeNode node in Children)
            {
                if (toPredict[FeatureDividedOn] == node.FeatureValueOfGroup)
                    return node.Predict(toPredict, out probability);
            }
            //Log.Warning("The provided example does not match any of the learned rules! Picking random rule instead.");
            return Children[0].Predict(toPredict, out probability);
        }

        /// <summary>
        /// Takes this branch, and divides it, returning whether or not we divided,
        /// </summary>
        public void Divide(int MinBucketSize, int MaximumDepth, DecisionTreeType TreeType, Random rng)
        {
            //First make sure  we should divide
            if (CurrentGroup == null || CurrentGroup.Count < MinBucketSize || CurrentDepth >= MaximumDepth)
            {
                var group = CurrentGroup.GroupBy(x => x.Predictor).OrderByDescending(y => y.Count()).First();
                Prediction = group.Key;
                Probability = group.Count() / (float)CurrentGroup.Count;
                //CurrentGroup = null;
                leaf = true;
                return;
            }
            if (TreeType != DecisionTreeType.RandomNode)
            {
                //Then we calculate the Info gain for all features
                Dictionary<int, float> InfoGains = new Dictionary<int, float>();
                for (int i = 0; i < CurrentGroup[0].FeatureValues.Length; i++)
                {
                    InfoGains.Add(i, CurrentGroup.InformationGain(i));
                }
                //Here we find which feature to divide on corresponding to our tree type
                FeatureDividedOn = FeatureToDivideOn(InfoGains, TreeType);
            }
            else
            {
                FeatureDividedOn = rng.Next(0, CurrentGroup[0].FeatureValues.Length);
            }

            //So now we need to group our values by this feature
            var Groups = CurrentGroup.GroupBy((x => x.FeatureValues[FeatureDividedOn]));

            foreach (var group in Groups)
            {
                //Create a new Node for this
                DecisionTreeNode child = new DecisionTreeNode();
                child.FeatureDividedOn = -1;
                List<ShortExample> examples = group.ToList();
                child.FeatureValueOfGroup = group.Key;
                child.CurrentGroup = examples;
                child.CurrentDepth = CurrentDepth + 1;
                //And add it to our children
                Children.Add(child);
            }

            //once all our children are created, we empty our current group
            CurrentGroup = null;
            foreach (DecisionTreeNode node in Children)
            {
                node.Divide(MinBucketSize, MaximumDepth, TreeType, rng);
            }

        }

        #endregion

        #region Helpers

        /// <summary>
        /// Decides which feature to divide on given the type of tree this is
        /// </summary>
        private static int FeatureToDivideOn(Dictionary<int, float> InfoGains, DecisionTreeType type)
        {
            if (type == DecisionTreeType.BestNode)
                return InfoGains.OrderByDescending(x => x.Value).ToArray()[0].Key;
            else
                return InfoGains.OrderByDescending(x => x.Value).ToArray()[0].Key;
        }

        #endregion

    }
}
