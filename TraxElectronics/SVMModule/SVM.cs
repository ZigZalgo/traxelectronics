﻿/*
 * Copyright 2019 VizworX Inc.
 * All rights reserved
 */
using Encog.ML.Data;
using Encog.ML.Data.Basic;
using Encog.ML.SVM;
using Encog.ML.SVM.Training;
using Encog.ML.Train;
using Encog.Persist;
using Encog.Util.Simple;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace TraxElectronics
{
    public class SVM
    {
        public SupportVectorMachine Machine;

        public SVM(int FeatureCount, SVMType SVMtype, KernelType Kerneltype)
        {
            Machine = new SupportVectorMachine(FeatureCount, SVMtype, Kerneltype);
        }

        public static SVM Load(string filePath)
        {
            FileInfo inf = new FileInfo(filePath);
            SVM ret = new SVM(0, SVMType.EpsilonSupportVectorRegression, KernelType.Linear);
            ret.Machine = (SupportVectorMachine)EncogDirectoryPersistence.LoadObject(inf);
            return ret;
        }

        public static void Save(SVM svm, string filePath)
        {
            FileInfo inf = new FileInfo(filePath);
            EncogDirectoryPersistence.SaveObject(inf, svm);
        }

        /// <summary>
        /// Used to learn a data set
        /// </summary>
        public void Train(List<float[]> Inputs, int epochCount)
        {
            Log.Message("Starting Training on SVM.");
            Log.Message("SVM Type : " + Machine.SVMType.ToString());
            Log.Message("Kernel Type: " + Machine.SVMType.ToString());
            Log.Message("Epoch Count: " + epochCount);

            //Formatting inputs because this library is dumb
            List<double[]> inputOnly =
                Inputs.Select(
                    (x) =>
                    {
                        var trans = new double[x.Length - 1];
                        for (int i = 0; i < x.Length - 1; i++)
                        {
                            trans[i] = x[i];
                        }
                        return trans;
                    }).ToList();
            List<double[]> outputOnly =
                Inputs.Select(
                    (x) =>
                    {
                        return new double[1] { x[x.Length - 1] };
                    }).ToList();

            // create training data
            IMLDataSet trainingSet = new BasicMLDataSet(inputOnly.ToArray(), outputOnly.ToArray());

            // train the SVM
            IMLTrain train = new SVMSearchTrain(Machine, trainingSet);

            for (int i = 0; i < epochCount; i++)
            {
                train.Iteration();
                Log.Message("Epoch: " + i + " done. Error achieved: " + train.Error);
            }

            Log.Message("Done Learning...");
        }

        public DrillState Predict(float[] toPredict)
        {
            BasicMLData dat = new BasicMLData(toPredict.Select(x => (double)x).ToArray());
            return (DrillState)Machine.Classify(dat);
        }

    }
}
