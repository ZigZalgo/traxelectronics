﻿/*
 * Copyright 2019 VizworX Inc.
 * All rights reserved
 */
using System;
using System.Collections.Generic;
using System.Linq;

namespace TraxElectronics
{
    public static class Helpers
    {

        #region Generic Helpers

        /// <summary>
        /// Splits an array into several smaller arrays.
        /// </summary>
        public static IEnumerable<IEnumerable<T>> Split<T>(this IEnumerable<T> array, int size)
        {
            for (var i = 0; i < array.Count() / size; i++)
            {
                yield return array.Skip(i * size).Take(size);
            }
        }

        /// <summary>
        /// Extract method to remove and return items from a list
        /// </summary>
        public static T Extract<T>(this List<T> input, int index)
        {
            T result = input[index];
            input.RemoveAt(index);
            return result;
        }

        /// <summary>
        /// Fischer-Yates shuffle of an array
        /// </summary>
        public static void Shuffle<T>(this T[] array, System.Random rng)
        {
            int n = array.Length;
            for (int i = 0; i < n; i++)
            {
                // Use Next on random instance with an argument.
                // ... The argument is an exclusive bound.
                //     So we will not go past the end of the array.
                int r = i + rng.Next(n - i);
                T t = array[r];

                array[r] = array[i];

                array[i] = t;

            }
        }

        #endregion

        #region Encoding

        /// <summary>
        /// Takes an IDataCollection, and turns it into a coded "2D Array" of uints
        /// </summary>
        public static Dictionary<float, ushort>[] Encode(this List<FloatExample> input, int BucketSize)
        {
            Log.Message("\tEncoding...");

            //numeric codebooks
            Dictionary<float, ushort>[] codeBook = new Dictionary<float, ushort>[input[0].FeatureValues.Length];

            //First we generate codebooks for all the features
            for (int i = 0; i < input[0].FeatureValues.Length; i++)
                codeBook[i] = CodifyFeature(input.GetColumn(i), BucketSize);

            return codeBook;
        }

        /// <summary>
        /// Turns an array of numerical feature values into codes
        /// </summary>
        private static Dictionary<float, ushort> CodifyFeature(float[] featureValues, int buckets)
        {
            Dictionary<float, ushort> codeBook = new Dictionary<float, ushort>();

            //Find the bounds of the buckets
            float min = featureValues.Percentile(.9);
            float max = featureValues.Percentile(.1);

            //If there is no difference between the minimum and the maximimum, everything is coded to 0
            if (max - min == 0)
            {
                codeBook.Add(max, 0);
            }
            //Else, we create a code for every value
            else
            {
                //Add each bin
                float bucketSize = (max - min) / buckets;
                for (int i = 0; i < buckets; i++)
                {
                    //This bin gets codded to i
                    codeBook.Add(((i * bucketSize) + min), (ushort)i);
                }
            }
            return codeBook;
        }

        /// <summary>
        /// Takes an example, and converts it to a coded example
        /// </summary>
        private static ShortExample CodifyExample(FloatExample ex, Dictionary<float, ushort>[] codeBook)
        {
            ushort[] ushorts = new ushort[ex.FeatureValues.Length];
            //For each feature value
            for (int i = 0; i < ex.FeatureValues.Length; i++)
            {
                //Grab the corresponding codebook for this feature
                Dictionary<float, ushort> numBook = codeBook[i];
                //First we make sure that the float is not nonsense
                float val = ex.FeatureValues[i];
                if (
                float.IsNaN((float)val) ||
                float.IsInfinity((float)val)
                )
                {
                    //If it is, we code it to the nonsense code
                    ushorts[i] = ushort.MaxValue;
                    continue;
                }

                //Then we need to find the closest bin to the value
                float tempKey = numBook.Keys.ToList()[0];
                double distToTemp = Math.Abs(tempKey - val);

                //For each bin, we see how close we are to it
                foreach (float key in numBook.Keys)
                {
                    double newDist = Math.Abs(key - val);
                    //And we update if closer than the previous close
                    if (newDist < distToTemp)
                    {
                        tempKey = key;
                        distToTemp = newDist;
                    }
                }
                //And store the code
                ushorts[i] = numBook[tempKey];

            }
            ShortExample retVal = new ShortExample(ex.ID, ushorts, ex.Predictor);
            return retVal;
        }

        /// <summary>
        /// Removes the bottom and top percentiles from an array of floats
        /// </summary>
        public static IEnumerable<float> RemovePercentiles(this IEnumerable<float> featureValues, double MinPercentile, double MaxPercentile)
        {
            //Find the bounds of the buckets
            float min = featureValues.Percentile(MinPercentile);
            float max = featureValues.Percentile(MaxPercentile);
            List<float> retVal = new List<float>();
            for(int i = 0; i < featureValues.Count(); i++)
            {
                if (featureValues.ElementAt(i) >= max || featureValues.ElementAt(i) <= min)
                    continue;
                retVal.Add(featureValues.ElementAt(i));
            }

            return retVal;
        }

        /// <summary>
        /// Returns the value for which x percentile of the array is less than or equal to
        /// </summary>
        public static float Percentile(this IEnumerable<float> values, double percentile)
        {
            float[] featureValues = values.ToArray();
            List<float> floatList = new List<float>();
            for (int i = 0; i < featureValues.Length; i++)
            {
                float fv = featureValues[i];
                if (float.IsNaN(fv) ||
                    float.IsInfinity(fv))
                {
                    continue;
                }
                floatList.Add(fv);

            }
            float[] cloned = floatList.ToArray();
            Array.Sort(cloned);
            int N = cloned.Length;
            float n = (float)((N - 1) * percentile + 1);
            // Another method: double n = (N + 1) * excelPercentile;
            if (n == 1d)
                return cloned[0];
            else if
                (n == N) return cloned[N - 1];
            else
            {
                int k = (int)n;
                float d = n - k;
                return cloned[k - 1] + d * (cloned[k] - cloned[k - 1]);
            }
        }

        /// <summary>
        /// Retrieves the column of a list of examples
        /// </summary>
        public static float[] GetColumn(this List<FloatExample> input, int column)
        {
            float[] retVal = new float[input.Count];
            for (int i = 0; i < input.Count; i++)
            {
                retVal[i] = input[i].FeatureValues[column];
            }
            return retVal;
        }

        #endregion

        #region Discretizing

        /// <summary>
        /// Takes an IDataCollection, and turns it into a coded "2D Array" of uints
        /// </summary>
        public static ShortExample[] Discretize(this List<FloatExample> input, Dictionary<float, ushort>[] codeBook)
        {
            Log.Message("\tDiscretizing...");
            List<ShortExample> retVal = new List<ShortExample>();
            for (int i = 0; i < input.Count; i++)
            {
                retVal.Add(CodifyExample(input[i], codeBook));
                if (i % 100000 == 0)
                {
                    Log.Message("\t\t" + (i / (float)input.Count) + " complete");
                }
            }
            Log.Message("\tDiscretizing Complete");
            return retVal.ToArray();
        }

        /// <summary>
        /// Discretizes a single float array using a codebook
        /// </summary>
        public static ShortExample Discretize(this FloatExample input, Dictionary<float, ushort>[] codeBook)
        {
            return CodifyExample(input, codeBook);
        }

        #endregion

        /// <summary>
        /// Calculates the information gain from feat2 if divided by the attributes of feat1
        /// </summary>
        public static float InformationGain(this List<ShortExample> Examples, int feat1)
        {
            //First we calculate the entropy of feature 2
            float before = Examples.PredictorEntropy();
            float after = 0;
            //Then we group all the examples into their corresponding feature 1
            var groups = Examples.GroupBy(x => x.FeatureValues[feat1]);
            foreach (var group in groups)
            {
                //And add their entropy to the total entropy of after dividing
                after += group.ToList().PredictorEntropy();
            }
            //Return the difference in entropy
            return after - before;
        }

        /// <summary>
        /// Calculates the shannon entropy of a feature value array
        /// </summary>
        public static float PredictorEntropy(this List<ShortExample> Examples)
        {
            double entropy = 0;
            //Group by each feature
            var groups = Examples.GroupBy(x => x.Predictor);

            /*
             * Shannon entropy is defined as
             * Σ p(x) * log p(x)
             * for all examples
             */

            //For each group
            foreach (var group in groups)
            {
                //The probability of this key is the group count div the total
                //This is 'p(X)'
                float probX = group.Count() / (float)Examples.Count();
                //Now we take the log to get "log p(x)'
                double logProbX = Math.Log(probX, 2);
                //The product of these two is 'p(x) * log p(x)' 
                double Product = probX * logProbX;
                //And we know there are as many examples like this as there are in the group
                //so we can multiply this by the count to get the partial sum
                double Sum = Product * group.Count();
                //And add it
                entropy += Sum;
            }
            return (float)entropy;
        }

    }
}

