﻿/*
 * Copyright 2019 VizworX Inc.
 * All rights reserved
 */
using System;
using System.Collections.Generic;
using System.IO;
using TraxElectronics;

namespace SamplePrediction
{
    public class Program
    {

        public static Random rng = new Random(1337);
        public static void Main(string[] args)
        {
            //Set the Log here to output to the specified file, as well as to console
            Log.SetStream(new FileStream(Path.Combine(args[1], "Log.txt"), FileMode.Create), true);
            SampleTraining(args[0], args[1]);
            //SampleTesting("<Directory containg the csv(s) you want to test>", "<Directory the tree was saved to>");
            //SamplePrediction(input, "<Directory the tree was saved to>");

            //Close the logging stream
            Log.EndStream();
        }

        /// <summary>
        /// Sample usage for training a new tree
        /// </summary>
        static void SampleTraining(string FileDirectory, string TreeDirectory)
        {
            Log.Message("Generating Tree...");
            //Tell the parser which directory to read the inputs from
            Parser p = new Parser(FileDirectory);
            //Tell the parser how many files in the directory do you want to sample (leave empty to parse all)
            List<float[]> inputs = p.ParseAllList();
            //Encode the inputs using a bin size of 100 (https://en.wikipedia.org/wiki/Data_binning for a more detailed explanation)
            Dictionary<int, Dictionary<float, byte>> codeBook = inputs.Encode(100);
            //Code the inputs using the generated codebook
            List<byte[]> discrete = inputs.Discretize(codeBook);
            //This step simply clears memory because we don't need to float values anymore
            inputs = null;
            //Tell the program to learn a decision tree of type Best node, with a max depth, and min bucket size
            DecisionTree tree = new DecisionTree(DecisionTreeType.BestNode, discrete[0].Length*2, 1000);
            //Train the program on the discretized inputs, while trying to learn which feature
            tree.Train(discrete, discrete[0].Length - 1);
            //Save the tree
            DecisionTree.Save(tree, codeBook, TreeDirectory);
        }

        /// <summary>
        /// Sample usage for testing a generated tree
        /// </summary>
        /// <param name="FileDirectory"></param>
        static void SampleTesting(string FileDirectory, string TreeDirectory)
        {
            Log.Message("Testing Tree...");
            //Tell the parser which directory to read the inputs from
            Parser p = new Parser(FileDirectory);
            //Tell the parser how many files in the directory do you want to sample (leave empty to parse all)
            List<float[]> inputs = p.ParseAllList();
            //Load a previously saved tree from the supplied path
            DecisionTree tree = DecisionTree.Load(TreeDirectory, out Dictionary<int, Dictionary<float, byte>> codeBook);
            //Bin the inputs using the learned codebook
            List<byte[]> discrete = inputs.Discretize(codeBook);
            float count = 0;
            //For each example found in the testing files
            foreach (byte[] sample in discrete)
            {
                //Try to predict it
                DrillState predicted = tree.Predict(sample, out float prob);
                //Count how many we got right
                if ((DrillState)sample[sample.Length - 1] == predicted)
                    count++;
            }
            //Return the total accuracy measure
            float acc = count / discrete.Count;
        }

        /// <summary>
        /// Sample usage for predicting a single input example
        /// </summary>
        static DrillState SamplePrediction(float[] inputs, string TreeDirectory)
        {
            //Load the tree from the path it was saved
            DecisionTree tree = DecisionTree.Load(TreeDirectory, out Dictionary<int, Dictionary<float, byte>> codeBook);
            //Predict the coded input (Prob here is how confident the algorithm is)
            return tree.Predict(inputs.Discretize(codeBook), out float prob);
        }
    }
}

