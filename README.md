# Trax Electronics Classifier
The TraxElectronics Classifier is a program built to use multiple learning methods to attempt to predict the drill state of a drill bit.

### Learners
The classifier currently houses two learners; a support vector machine classifier, and a decision tree classifier. The hope is that in the future, an RNN, and perhaps some other classifiers will be included to predict using an ensemble method, however the current accuracy is suitable for our desires. The support vector machine uses the library provided by [Encog.NET](https://github.com/encog/encog-dotnet-core). The decision tree was built by Cooper Davies from here at VizworX. Contact info for him is at the bottom of the readme.

The program was built to be as simple to plug-and-play as possible. There are technically two components for each learner: The training, and the classifying. The following sections will be dedicated to explaining how to get this library up and running.

In this document we will go over how to instantiate the decision tree only. The SVM performed less than ideal and the exact piping of data is almost identical to that of the decision tree so we will simply omit the specifics.

## Parsing
The first aspect of this library requires first loading in all the examples to train from. This is done using the Parser class. This class is multi-threaded to allow for quickly loading in multiple csv files simultaneously.

### Method Calls
You must dictate to the parser where the files you want to load from exist. This is done by first instantiating the parser object with the desired directory
`public Parser(string directoryPath)`
The calling code can then specify the number of files to parse
`parser.ParseAllList(5);`

ParseAllList lets you specify how many files to parse to help allow random sampling occur for large datasets. For example, if we had 1000 files, and wanted to train on half of them, and validate on the other half, we can specify that we would like half the files to be parsed at a time.

To parse all the files, simply omit the integer argument.

### CSV Format

The parser works by first looking for expected words in the header file, then associating the word with an index to make sure that the order of the headers in the input file does not matter. What does matter is that the correct headers are provided per each file within the directory. We include the headers in the following table, along with the data types which must be used for the values in each column under the header.

 | Header| Data Type |
 |-----------|-------|
 | HookLoad0 | float |
 |SurfaceTorque0| float|
 |WOB0 | float|
|ROP0 | float|
|SurfaceRotation0 | float|
|BlockHeight0 | float|
|BitDepth0 | float|
|HoleDepth0 | float|
|BitDepthTVD0 | float|
|MotorDP0 | float|
|Inclination0 | float|
|ContinuousInclination0 | float|
|GravityToolFace0 | float|
|MagneticToolFace0 | float|
|MakingHole | {0,1}|
|DrillingRotating | {0,1}|
|DrillingSliding | {0,1}|

For example, a sample csv may look like:

| DeltaT[sec] | HookLoad0[N] | SurfaceTorque0[ft*lbs] | WOB0[N] | ROP0[m/hr] |...|
|--------------------|-------------|--------------|------------------------|---------|------------|--------|
| 43052.3489583 | 0 | 43570.32 | 0 | 0  |...||--------------------|
|...|...|...|...|...|...|...|

although no specific ordering of the fields is required.

### Memory Representation
If you look at the return type of the `parser.ParseAllList(5);` method call you will notice that it returns an object of type `FloatExample`. This is an easy way to represent each row found in the csv by implicitly storing information as opposed to explicitly. The `FloatExample` class is actually an inherited extension of the `AbstractExample` class. An `AbstractExample` object stores an ID, and a predictor (the classification of this example). We can express an example in a multitude of ways based on how we wish to compress the data.

For example we read the exact data in as floats to receive a `FloatExample` list. We further discretize ([bin](https://en.wikipedia.org/wiki/Data_binning)) the data  to make it both discrete, but also compressed. We can do this by first calling `.Encode(1000)` on a `FloatExample` list. This method call will output a `Dictionary<float, ushort>[]` which will be used to represent what float represents what encoding for each header. The value 1000 indicates that we are discretizing using 1000 bins of fixed width. Keep this "codebook" as a variable because it will need to be used later.

After creating a codebook from encoding, calling `.discretize(codebook)` on the `FloatExample` list will create a `ShortExample` list which uses half the memory. In this method call, `codebook` is the  `Dictionary<float, ushort>[]` generated form calling encode. Because the decision trees require information gain to compute (and because information gain requires discrete data) only a `ShortExample` list can be learned with. 
<br>
To predict a sample value of floats at runtime, the encoding needs to happens as well, but how to do so will be explained in a later section.

## Training
Now that we have parsed and discretized the data, we will present how to instantiate a new decision tree and then train it. Before we begin, there are a few variables you should familiarize yourself with.

### Variables
1) Max Bucket Size 
&nbsp;&nbsp;&nbsp;&nbsp; When a decision tree attempts to learn, it takes the examples found at a given node, and decides whether or not to make its rules more granular (specific). If it decides to do so, it will split itself into smaller sub-nodes. The max bucket size indicates how large a node is allowed to be before it is forced to divide and learn more specific rules. 
2) Tree Type
&nbsp;&nbsp;&nbsp;&nbsp; The tree type indicates what kind of rules the decision tree should try to learn. There are three types {BestNode, WorstNode, and RandomNode}. Respectively these three indicate that the rules should be created from features which provide the most information, features which provide the least information, and a random feature. The best and worst node variants are the slowest but tend to be more accurate while the random node is significantly quicker to learn.
3) Max Depth
&nbsp;&nbsp;&nbsp;&nbsp; Max depth can simply be represented as how many rules you want the tree to learn. The deeper the tree is allowed to go, the more rules it will create. Beware of setting this too high though. The higher the depth, the more time and memory must be used to both learn and store the rules. You also see diminishing returns per new rule as there becomes less and less to learn as we create more rules.

### Learning
To create a new tree, we must specify with the constructor each of the above variables.

`DecisionTree tree = new DecisionTree(DecisionTreeType.BestNode, 12, 1000);`
In the above example we are creating a tree which tries to learn using the most informative features, with a max depth of 12 rules, and one which must continue learning if it contains 1000 samples at any given node.

Now we simply tell the tree to learn on the discretized inputs with `tree.Train(discretizedInputs);`

## Predicting

Once a model is trained, you are able to simply feed it a sample input type. For the decision tree, you will once again need to bin the input using the codebook provided by the discretizer.

`ShortExample shortExample=floatExample.Discretize(codeBook);`

Once our float example has been discretized we can try to predict it.

`tree.Predict(shortExample.FeatureValues, out float prob);`

Notice that we use the `.FeatureValues` property of the short example. This is because `.Predict()` uses a `short[]` as input. Notice that the decision tree is able to supply a confidence value using the `out prob` value. If you omit this argument, it will still work, but you wont receive the confidence.

## Saving/Loading

Models can be saved/loaded from memory so that we can store/retrieve models after we have learned them. This repository contains a fully trained SVM, as well as Best Decision Tree.

You can save a trained tree by calling
`DecisionTree.Save(tree, codebook, directoryToSave)`
Here one of the arguments is the codebook. This is so that you can encode future examples after the training has been completed, and if you have lost the in-memory structure of the codebook.

Loading a decision tree is done similarly
`DecisionTree.Load(directoryToLoad, out codebook)`
Here it outs the loaded codebook to use for discretizing examples with.

## Examples
In the code base the class Testing.cs contains three examples. The first function(`SampleValidation`) shows how to load a tree to classify a list of examples parsed from a directory. The second (`SampleTraining`) shows how to load examples from a directory to train a decision tree. The final function (`SamplePrediction`) is an example of how you might use this in a development environment as a 3rd party library.

#### Contact Info:

Cooper Davies - cooper.davies@agilesoftwareenginnering.org

email if you have an questions regarding usage/alteration

